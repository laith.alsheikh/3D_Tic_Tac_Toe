/**
 * This is an "abstract" class - you can't declare an object of this type.
 * It exists only to provide the one static method below. Call
 *    WinLines.generateWinningLines(n)
 * to get a Coordinate[][] array containing all of the possible winning
 * lines, one per row, on an n by n by n 3D tic-tac-toe board.
 */
public abstract class WinLines {
  
  public static Coordinate[][] generateWinningLines(int n){
    /* Generate a 2D array of coordinates containing all of the possible
     * ways to get "n in a row" in an n by n by n 3D tic-tac-toe board.
     * There will be 3n^2+6^n+4 such winning lines, one per row, each
     * containing n Coordinates, one per column.
     */
    Coordinate[][] result = new Coordinate[3*n*n+6*n+4][n];
    int nextSpot=0;
    
    //lines where only the column number changes
    for(int lvl=0; lvl<n; lvl++)
      for(int row=0; row<n; row++){
         for(int i=0; i<n; i++)
            result[nextSpot][i] = new Coordinate(lvl,row,i);
         nextSpot++;
      }
    
    //lines where only the row number changes
    for(int lvl=0; lvl<n; lvl++)
      for(int col=0; col<n; col++){
         for(int i=0; i<n; i++)
            result[nextSpot][i] = new Coordinate(lvl,i,col);
         nextSpot++;
      }
   
    //lines where only the level number changes
    for(int row=0; row<n; row++)
      for(int col=0; col<n; col++){
         for(int i=0; i<n; i++)
            result[nextSpot][i] = new Coordinate(i,row,col);
         nextSpot++;
      }
   
    //diagonals within a particular level
    for(int lvl=0; lvl<n; lvl++){
      for(int i=0; i<n; i++){
        result[nextSpot][i] = new Coordinate(lvl,i,i);
        result[nextSpot+1][i] = new Coordinate(lvl,i,n-1-i);
      }
      nextSpot += 2;
    }
    
    //diagonals within a particular row
    for(int row=0; row<n; row++){
      for(int i=0; i<n; i++){
        result[nextSpot][i] = new Coordinate(i,row,i);
        result[nextSpot+1][i] = new Coordinate(i,row,n-1-i);
      }
      nextSpot += 2;
    }
    
    //diagonals within a particular column
    for(int col=0; col<n; col++){
      for(int i=0; i<n; i++){
        result[nextSpot][i] = new Coordinate(i,i,col);
        result[nextSpot+1][i] = new Coordinate(i,n-1-i,col);
      }
      nextSpot += 2;
    }
    
    //diagonals that go from corners to opposite corners
    for(int i=0; i<n; i++){
      result[nextSpot][i] = new Coordinate(i,i,i);
      result[nextSpot+1][i] = new Coordinate(i,i,n-1-i);
      result[nextSpot+2][i] = new Coordinate(i,n-1-i,i);
      result[nextSpot+3][i] = new Coordinate(n-1-i,i,i);
    }

    //That's all of them. Brute-force but effective. Return the result.
    return result;
  }//generateWinningLines()
  
}//WinLines class
