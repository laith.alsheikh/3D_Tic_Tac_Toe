import java.awt.Color;
import java.lang.*;

public class TTTBoard3D {
  
  //instance variables
  private int[][][] array;
  private Color player1;
  private Color player2;
  private Color empty;
  private String message;
  private int turn = 0;
  private Coordinate[][] waysToWin;
  private int size;
  private Color winLine;
  private boolean gameWon;
  
  
  
  
  public TTTBoard3D(int n)
  {
    array = new int[n][n][n]; 
    size = n;
  }//constructor method;
  
  public Color getSquareColor(Coordinate c)
  {
    //assigned colors for players, empty squares, and winning squares
    player1 = Color.RED;
    player2 = Color.BLUE;
    empty = Color.WHITE;
    winLine = Color.ORANGE;
    
    if(array[c.level][c.row][c.column] == 0) //check if square is empty
    {
      return empty; //set it to white
    }
    if(array[c.level][c.row][c.column] == 1) //check if player 1 clicked
    {
      return player1; //set it to red    
    }
    if(array[c.level][c.row][c.column] == 2) //check if player 2 clicked
    {
      return player2; //set it to blue
    }
    if(array[c.level][c.row][c.column] == 3) //check if its the winning line
    {
      return winLine; //set it to orange
    }
    else
    {
      return empty; //should not be used, but just in case
    }
  }//getSquareColor method
  
  public String getMessage() //returns message to user
  {
    int playerNum = winningPlayer(); //returns number of winning player
    if(boardFull() == true) //if there aren't any empty squares
    {
      return "The boards are full, game is over!";
    }    
    if(isWinner()) //if there is a winner
    {
      if(playerNum == 1) //if winner is player 1
      {
        return "Player 1 has won!";
      }
      if(playerNum == 2) //if winner is player 2
      {
        return "Player 2 has won!";
      }
      
    }
    if(gameWon == false) //if there is not a winner yet
    {
      //by getting the remainder, we can see if the number is even or odd
      if(turn%2 == 0) //if its an even number
      {
        return "It is Player 1's turn";
      }
      else if(turn%2 == 1) // if its an odd number
      {
        return "It is Player 2's turn";
      }
    } 
    
    return "";
    
  }//getMessage method
  
  public void handleClick(Coordinate c)
  {
    
    if(!gameWon) // if there is not a winner yet
    {
      if(boardFull() == false) //if there any empty cases left
      {
        if(checkEmpty(c) == true && turn%2 == 0) // check if its player 1 turn and the square is empty
        {
          array[c.level][c.row][c.column] = 1;
          turn++; //update turn
        }
        else if(checkEmpty(c) == true && turn%2 != 0) // check if its player 2 turn and the square is empty
        {
          array[c.level][c.row][c.column] = 2;
          turn++; //update turn
        }
        else 
        {
          return;
        }
      }
    }
  }//handClick method
   
  
  public boolean checkEmpty(Coordinate c)
  {
    if(array[c.level][c.row][c.column] == 0) //if square is empty
    {
      return true;
    }
    else
    {
      return false;
    }
  }//checkEmpty method
  
  
  
  public boolean isWinner()
  {
    waysToWin = WinLines.generateWinningLines(size); //array of winning lines
    int counter1 = 0; 
    int counter2 = 0;
    Coordinate temp;
    for(int i = 0; i < waysToWin.length; i++)
    {
      for(int j = 0; j < size; j++)
      {
       temp = waysToWin[i][j]; //one coordinate of a winning line
       if(array[temp.level][temp.row][temp.column] == 1) //if that coordinate square has a 1
       {
         counter1++; 
       }
       if(array[temp.level][temp.row][temp.column] == 2) //if that coordinate square has a 1
       {
         counter2++;
       }
      }    
      if(counter1 == size) //check if the winning line is all 1's
      {
        for(int k = 0; k < size; k++)
        {
          temp = waysToWin[i][k]; //coordinates of winning line
          array[temp.level][temp.row][temp.column] = 3; //change all values to 3 so color can be updated      
        }
        gameWon = true; 
        return gameWon;
      }
      else if(counter2 == size) //check if the winning line is all 2's
      {
        for(int k = 0; k < size; k++)
        {
          temp = waysToWin[i][k]; //coordinates of winning line
          array[temp.level][temp.row][temp.column] = 3; //change all values to 3 so color can be updated         
        }
        gameWon = true;
        return gameWon;
      }
      
      counter1 = 0; //reset counter1 if not all values of winning line are same
      counter2 = 0; //reset counter2 if not all values of winning line are same
    }
    
    return false;
    
  }//isWinner method
  
  public int winningRow() //returns the row of the winning line in the waysToWin array
  {
    waysToWin = WinLines.generateWinningLines(size);
    int counter1 = 0;
    int counter2 = 0;
    for(int i = 0; i < waysToWin.length; i++)
    {
      for(int j = 0; j < size; j++)
      {
       Coordinate temp = waysToWin[i][j];
       if(array[temp.level][temp.row][temp.column] == 1)
       {
         counter1++;
       }
       else if(array[temp.level][temp.row][temp.column] == 2)
       {
         counter2++;
       }
      }
      if(counter1 == size)
      {
        
        return i;
      }
      else if(counter2 == size)
      {
        return i;
      }
      
      counter1 = 0;
      counter2 = 0;
    }
    return -1;
  }//winningRow method
  
  public int winningPlayer() //returns which player won the game
  {
    waysToWin = WinLines.generateWinningLines(size);
    int counter1 = 0;
    int counter2 = 0;
    for(int i = 0; i < waysToWin.length; i++)
    {
      for(int j = 0; j < size; j++)
      {
       Coordinate temp = waysToWin[i][j];
       if(array[temp.level][temp.row][temp.column] == 1)
       {
         counter1++;
       }
       else if(array[temp.level][temp.row][temp.column] == 2)
       {
         counter2++;
       }
      }
      if(counter1 == size) //if the counter for player 1 has the same value has the size of the board
      {
        return 1; //it means player 1 has won
      }
      else if(counter2 == size) //if the counter for player 2 has the same value has the size of the board
      {
        return 2; //it means player 2 has won
      }
      counter1 = 0; //reset counter if not the case
      counter2 = 0;
    }
    return -1;
  }//winningPlayer method
  
  public boolean boardFull() //checks if the board is full
  {
    for(int lev = 0; lev < size; lev++)
    {
      for(int row = 0; row < size; row++)
      {
        for(int col = 0; col < size; col++)
        {
          if(squareEmpty(lev,row,col))
          {
            return false; //if the method detects an empty square, return false because board is not full yet
          }
        }
      }
    }
    return true; // if it does not find any empty squares, that means the board is full and return true
  }//boardFull method
  
  public boolean squareEmpty(int level, int row, int col)
  {
    return array[level][row][col] == 0; // if the square has a value of 0, it means its empty
  }//squareEmpty method
  
    
}
