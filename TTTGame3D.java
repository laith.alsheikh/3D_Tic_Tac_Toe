import java.util.Scanner;

public class TTTGame3D {
  
  private static int WINDOW_SIZE = 700;
  public static void main(String[] args) 
  {
    int n = getBoardSize();
    TTT3DWindow theWindow = new TTT3DWindow(WINDOW_SIZE,WINDOW_SIZE,n);
  }
  
  public static int getBoardSize() //gets desired board size from user
  {
    Scanner sc = new Scanner(System.in); //calls method to allow program to read user input
    int val = 0;
    int number = 0;
    while(val == 0)
    {
      System.out.println("Please enter a number between 3 and 6"); //inform user of acceptable inputs
      number = sc.nextInt(); //reads user input
      
      if(number < 3 || number > 6) // if its lower than 3 or more than 6
      {
        System.out.println("That is not a number between 3 and 6"); //repeat to user acceptable inputs
        number = sc.nextInt(); //rereads user input
      }
      if(number >= 3 && number < 7) // if its between 3 and 6
      {
        val = number; // save it as val so loop will end
      }
    }  
    
    return number; //return user input
  }
}



