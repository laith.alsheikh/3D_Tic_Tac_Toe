
public class Coordinate {
  
  public int level;
  public int row;
  public int column;
  
  public Coordinate(int levelNum, int rowNum, int columnNum)
  {
    level = levelNum;
    row = rowNum;
    column = columnNum;
  }//constructor method
  
  public String toString()
  {
    String format = "(" + level + "," + row + "," + column + ")";
    
    return format;
  }//toString method
  
  
}
